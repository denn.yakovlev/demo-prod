import unittest
import math_lib

class Testing(unittest.TestCase):
    def test_string(self):
        self.assertEqual(math_lib.mul(2,3), 6)
        self.assertEqual(math_lib.mul(3,3), 9)
        self.assertEqual(math_lib.mul(100,5), 500)

if __name__ == '__main__':
    unittest.main()
