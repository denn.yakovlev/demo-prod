.. demo_prod documentation master file, created by
   sphinx-quickstart on Tue Nov 19 14:07:36 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to demo_prod's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2
.. automodule:: math_lib
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

